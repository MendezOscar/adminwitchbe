using System.Linq;
using AdminWitchBackEnd.Data;
using AdminWitchBackEnd.Models;

namespace AdminWitchBackEnd.Repository
{
    public class TaskManager : IDataRepository<Task>
    {

        readonly AdminWitchContext _adminWitchContext;
        public TaskManager(AdminWitchContext adminWitchContext)
        {
            _adminWitchContext = adminWitchContext;
        }
        public void Add(Task entity)
        {
            _adminWitchContext.Task.Add(entity);
            _adminWitchContext.SaveChanges();
        }

        public void Delete(Task entity)
        {
            _adminWitchContext.Remove(entity);
            _adminWitchContext.SaveChanges();
        }

        public Task Get(int id)
        {
            return _adminWitchContext.Task
                  .FirstOrDefault(e => e.TaskId == id);
        }

        public System.Collections.Generic.IEnumerable<Task> GetAll()
        {
            return _adminWitchContext.Task.ToList();
        }

        public void Update(Task dbEntity, Task entity)
        {
            dbEntity.Name = entity.Name;
            dbEntity.Date = entity.Date;
            dbEntity.StatusTask = entity.StatusTask;
            dbEntity.OwnerId = entity.OwnerId;

            _adminWitchContext.SaveChanges();
        }
    }
}