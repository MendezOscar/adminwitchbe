using System.Collections.Generic;
using System.Linq;
using AdminWitchBackEnd.Data;
using AdminWitchBackEnd.Models;

namespace AdminWitchBackEnd.Repository
{
    public class CitizenManager : IDataRepository<Citizen>
    {
        readonly AdminWitchContext _adminWitchContext;

        public CitizenManager(AdminWitchContext adminWitchContext)
        {
            _adminWitchContext = adminWitchContext;
        }
        public void Add(Citizen entity)
        {
            _adminWitchContext.Citizen.Add(entity);
            _adminWitchContext.SaveChanges();
        }

        public void Delete(Citizen entity)
        {
            _adminWitchContext.Remove(entity);
            _adminWitchContext.SaveChanges();
        }

        public Citizen Get(int id)
        {
            return _adminWitchContext.Citizen
                  .FirstOrDefault(e => e.CitizenId == id);
        }

        public IEnumerable<Citizen> GetAll()
        {
            return _adminWitchContext.Citizen.ToList();
        }

        public void Update(Citizen dbEntity, Citizen entity)
        {
            dbEntity.FirstName = entity.FirstName;
            dbEntity.LastName = entity.LastName;
            dbEntity.DateOfBirth = entity.DateOfBirth;
            dbEntity.Age = entity.Age;
            dbEntity.DNI = entity.DNI;
            dbEntity.Address = entity.Address;
            dbEntity.PhoneNumber = entity.PhoneNumber;
            dbEntity.Gender = entity.Gender;
            dbEntity.CivilStatus = entity.CivilStatus;
            dbEntity.Education = entity.Education;

            _adminWitchContext.SaveChanges();
        }
    }
}