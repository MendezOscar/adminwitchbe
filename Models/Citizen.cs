using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminWitchBackEnd.Models
{
    public class Citizen
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CitizenId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Age { get; set;}
        public string DNI { get; set;}
        public string Address { get; set;}
        public string PhoneNumber { get; set;}
        public string Gender { get; set;}
        public string CivilStatus {get; set;}
        public string Education {get; set;}
        public ICollection<Task> Tasks { get; set; }

    }
}