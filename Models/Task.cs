using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminWitchBackEnd.Models
{
    public class Task
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TaskId { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public int StatusTask { get; set;}
        public int OwnerId { get; set;}
        public Citizen Citizen {get; set;}
    }
}