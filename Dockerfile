FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 5000
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /src
COPY ["AdminWitchBackEnd.csproj", "./"]
RUN dotnet restore "AdminWitchBackEnd.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "AdminWitchBackEnd.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "AdminWitchBackEnd.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "AdminWitchBackEnd.dll"]
