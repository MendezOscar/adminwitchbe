using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AdminWitchBackEnd.Data;
using AdminWitchBackEnd.Models;
using AdminWitchBackEnd.Repository;

namespace AdminWitchBackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly AdminWitchContext _context;
        private readonly IDataRepository<AdminWitchBackEnd.Models.Task> _dataRepository;


        public TaskController(IDataRepository<AdminWitchBackEnd.Models.Task> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        // GET: api/Task
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<AdminWitchBackEnd.Models.Task> task = _dataRepository.GetAll();
            return Ok(task);
        }

        // GET: api/Task/5
        [HttpGet("{id}", Name = "GetTask")]
        public IActionResult Get(int id)
        {
            AdminWitchBackEnd.Models.Task task = _dataRepository.Get(id);
            if (task == null)
            {
                return NotFound("The task record couldn't be found.");
            }
            return Ok(task);
        }

        // PUT: api/Task/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] AdminWitchBackEnd.Models.Task task)
        {
            if (task == null)
            {
                return BadRequest("Task is null.");
            }
            AdminWitchBackEnd.Models.Task taskToUpdate = _dataRepository.Get(id);
            if (taskToUpdate == null)
            {
                return NotFound("The task record couldn't be found.");
            }
            _dataRepository.Update(taskToUpdate, task);
            return NoContent();
        }

        // POST: api/Task
        [HttpPost]
        public IActionResult Post([FromBody] AdminWitchBackEnd.Models.Task task)
        {
            if (task == null)
            {
                return BadRequest("Citizen is null.");
            }
            _dataRepository.Add(task);
            return CreatedAtRoute(
                  "Get",
                  new { Id = task.TaskId },
                  task);
        }

        // DELETE: api/Task/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            AdminWitchBackEnd.Models.Task task = _dataRepository.Get(id);
            if (task == null)
            {
                return NotFound("The task record couldn't be found.");
            }
            _dataRepository.Delete(task);
            return NoContent();
        }
    }
}
