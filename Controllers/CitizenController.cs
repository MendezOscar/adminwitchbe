using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AdminWitchBackEnd.Data;
using AdminWitchBackEnd.Models;
using AdminWitchBackEnd.Repository;

namespace AdminWitchBackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitizenController : ControllerBase
    {
        private readonly IDataRepository<Citizen> _dataRepository;

        public CitizenController(IDataRepository<Citizen> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        // GET: api/Citizen
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Citizen> citizen = _dataRepository.GetAll();
            return Ok(citizen);
        }

        // GET: api/Citizen/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(int id)
        {
            Citizen citizen = _dataRepository.Get(id);
            if (citizen == null)
            {
                return NotFound("The Citizen record couldn't be found.");
            }
            return Ok(citizen);
        }

        // PUT: api/Citizen/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Citizen citizen)
        {
            if (citizen == null)
            {
                return BadRequest("Citizen is null.");
            }
            Citizen CitizenToUpdate = _dataRepository.Get(id);
            if (CitizenToUpdate == null)
            {
                return NotFound("The citizen record couldn't be found.");
            }
            _dataRepository.Update(CitizenToUpdate, citizen);
            return NoContent();
        }

        // POST: api/Citizen
        [HttpPost]
        public IActionResult Post([FromBody] Citizen citizen)
        {
            if (citizen == null)
            {
                return BadRequest("Citizen is null.");
            }
            _dataRepository.Add(citizen);
            return CreatedAtRoute(
                  "Get",
                  new { Id = citizen.CitizenId },
                  citizen);
        }

        // DELETE: api/Citizen/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Citizen citizen = _dataRepository.Get(id);
            if (citizen == null)
            {
                return NotFound("The citizen record couldn't be found.");
            }
            _dataRepository.Delete(citizen);
            return Ok(citizen);
        }

    }
}
