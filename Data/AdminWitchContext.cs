using System.ComponentModel.DataAnnotations.Schema;
using AdminWitchBackEnd.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AdminWitchBackEnd.Data
{
    public class AdminWitchContext : DbContext
    {
        public AdminWitchContext(DbContextOptions<AdminWitchContext> options) : base(options)
        { }
        public virtual DbSet<Citizen> Citizen { get; set; }
        public virtual DbSet<Task> Task { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>()
                .HasOne(c => c.Citizen)
                .WithMany(t => t.Tasks)
                .HasForeignKey(s => s.OwnerId);
        }
    }
}